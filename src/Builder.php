<?php

namespace Drupal\roles_permission_builder;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Service class to create roles and assign permissions.
 */
class Builder {

  /**
   * The entity storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $roleStorage;

  /**
   * The entity storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface|null
   */
  protected ?EntityStorageInterface $groupRoleStorage = NULL;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * The extension path resolver service.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected ExtensionPathResolver $extensionPathResolver;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Constructs the RolePermissions service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entity type bundle info service.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extensionPathResolver
   *   The extension path resolver service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityTypeBundleInfoInterface $entityTypeBundleInfo, ExtensionPathResolver $extensionPathResolver, ConfigFactoryInterface $configFactory, ModuleHandlerInterface $moduleHandler) {
    $this->roleStorage = $entityTypeManager->getStorage('user_role');
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->extensionPathResolver = $extensionPathResolver;
    $this->configFactory = $configFactory;
    $this->moduleHandler = $moduleHandler;

    if ($this->moduleHandler->moduleExists('group')) {
      $this->groupRoleStorage = $entityTypeManager->getStorage('group_role');
    }
  }

  /**
   * Creates roles and grants permissions for a module.
   *
   * @param string $module
   *   The module for which to create roles and grant permissions.
   * @param bool $contentAccessAvailable
   *   Flag indicating if the content_access module is available.
   * @param bool $rolesOnly
   *   Set to TRUE if you only want to create the roles.
   */
  public function createAndGrant(string $module, bool $contentAccessAvailable, bool $rolesOnly): void {
    $filename = $this->extensionPathResolver->getPath('module', $module) . '/config/roles_permissions.yml';
    if (!file_exists($filename)) {
      return;
    }
    $data = Yaml::decode(file_get_contents($filename));

    foreach ($data as $roleID => &$definition) {
      if ($roleID === '_content_access_per_node') {
        continue;
      }
      if ($roleID === '_groups') {
        if ($this->groupRoleStorage === NULL) {
          continue;
        }
        foreach ($definition as $groupType => &$groupTypeDefinition) {
          foreach ($groupTypeDefinition as $key => &$groupDefinition) {
            if (!in_array($groupDefinition['scope'] ?? 'individual', ['insider', 'outsider', 'individual'])) {
              continue;
            }
            $groupRoleID = implode('-', [$groupType, $key]);
            /** @var \Drupal\group\Entity\GroupRoleInterface|null $role */
            $role = $this->groupRoleStorage->load($groupRoleID);
            if ($role === NULL) {
              /** @var \Drupal\group\Entity\GroupRoleInterface $role */
              $role = $this->groupRoleStorage->create([
                'id' => $groupRoleID,
                'label' => 'placeholder',
                'group_type' => $groupType,
              ]);
            }
            $role->set('label', $groupDefinition['label'] ?? $groupRoleID);
            $role->set('weight', $groupDefinition['weight'] ?? 0);
            $role->set('admin', $groupDefinition['admin'] ?? FALSE);
            $role->set('scope', $groupDefinition['scope'] ?? 'individual');
            $role->set('global_role', $groupDefinition['global_role'] ?? NULL);
            $role->save();
            $groupDefinition['role'] = $role;
          }
          unset($groupDefinition);
        }
        unset($groupTypeDefinition);
        continue;
      }
      /** @var \Drupal\user\Entity\Role|null $role */
      $role = $this->roleStorage->load($roleID);
      if ($role === NULL) {
        /** @var \Drupal\user\Entity\Role $role */
        $role = $this->roleStorage->create([
          'id' => $roleID,
          'label' => $definition['label'] ?? $roleID,
        ]);
      }
      if (isset($definition['label'])) {
        $role->set('label', $definition['label']);
      }
      if (isset($definition['weight'])) {
        $role->set('weight', $definition['weight']);
      }
      $role->save();
      $definition['role'] = $role;
    }
    unset($definition);

    if ($rolesOnly) {
      return;
    }

    $contentTypesPerNodeAccess = [];
    $allContentTypes = array_keys($this->entityTypeBundleInfo->getBundleInfo('node'));
    if ($contentAccessAvailable) {
      $contentAccess = [];
      foreach ($allContentTypes as $contentType) {
        $contentAccess[$contentType] = [];
        foreach ([
          'view',
          'view_own',
          'update',
          'update_own',
          'delete',
          'delete_own',
        ] as $operation) {
          $contentAccess[$contentType][$operation] = [];
        }
      }
    }

    foreach ($data as $roleID => $definition) {
      if ($roleID === '_content_access_per_node') {
        $contentTypesPerNodeAccess = $definition;
        continue;
      }
      if ($roleID === '_groups') {
        if ($this->groupRoleStorage === NULL) {
          continue;
        }
        foreach ($definition as $groupType => $groupTypeDefinition) {
          foreach ($groupTypeDefinition as $key => $groupDefinition) {
            if (!isset($groupDefinition['role'])) {
              continue;
            }
            $role = $groupDefinition['role'];
            foreach ($groupDefinition['permissions'] ?? [] as $permission) {
              $role->grantPermission($permission);
            }
            try {
              $role->save();
            }
            catch (EntityStorageException) {
              // @todo log this exception.
            }
          }
        }
        continue;
      }
      $role = $definition['role'];
      foreach ($definition['permissions'] ?? [] as $permission) {
        $role->grantPermission($permission);
      }
      if ($contentAccessAvailable && isset($definition['content_access'])) {
        foreach ($definition['content_access'] as $operation => $contentTypes) {
          if ($contentTypes === 'any') {
            $contentTypes = $allContentTypes;
          }
          elseif (is_string($contentTypes)) {
            $contentTypes = [$contentTypes];
          }
          $op = ($operation === 'update') ? 'edit' : $operation;
          foreach ($contentTypes as $contentType) {
            $contentAccess[$contentType][$operation][] = $roleID;
            if (!in_array($op, ['view', 'view_own'])) {
              $role->grantPermission($op . ' any ' . $contentType . ' content');
            }
          }
        }
      }
      try {
        $role->save();
      }
      catch (EntityStorageException) {
        // @todo log this exception.
      }
    }

    if ($contentAccessAvailable) {
      $contentAccessConfig = $this->configFactory->getEditable('content_access.settings');
      $storedContentAccess = $contentAccessConfig->get('content_access_node_type') ?? [];
      foreach ($allContentTypes as $contentType) {
        $storedSettings = unserialize($storedContentAccess[$contentType] ?? '[]', ['']);
        $def = $contentAccess[$contentType] ?? [];
        if (in_array($contentType, $contentTypesPerNodeAccess, TRUE)) {
          $def['per_node'] = TRUE;
        }
        foreach ($def as $op => $roles) {
          $newRoles = array_merge($roles, $storedSettings[$op] ?? []);
          $def[$op] = array_unique($newRoles);
        }
        $storedContentAccess[$contentType] = serialize($def);
      }
      $contentAccessConfig
        ->set('content_access_node_type', $storedContentAccess)
        ->save();
    }
  }

  /**
   * Revokes all permissions for a role.
   *
   * @param string $roleId
   *   The role for which to revoke all permissions.
   */
  public function revokePermissions(string $roleId): void {
    /** @var \Drupal\user\Entity\Role|null $role */
    $role = $this->roleStorage->load($roleId);
    if ($role === NULL) {
      return;
    }
    foreach ($role->getPermissions() as $permission) {
      $role->revokePermission($permission);
    }
    try {
      $role->save();
    }
    catch (EntityStorageException) {
      // @todo log this exception.
    }
  }

  /**
   * Revokes all permissions from a group role.
   *
   * @param string $roleId
   *   The group role for which to revoke all permissions.
   */
  public function revokeGroupPermissions(string $roleId): void {
    /** @var \Drupal\group\Entity\GroupRoleInterface|null $role */
    $role = $this->groupRoleStorage->load($roleId);
    if ($role === NULL) {
      return;
    }
    foreach ($role->getPermissions() as $permission) {
      $role->revokePermission($permission);
    }
    try {
      $role->save();
    }
    catch (EntityStorageException) {
      // @todo log this exception.
    }
  }

  /**
   * Rebuilds all roles and permissions.
   *
   * @param bool $rebuildNodeAccess
   *   Set to TRUE if you also want to rebuild Drupal core's node access.
   * @param bool $rolesOnly
   *   Set to TRUE if you only want to create the roles.
   */
  public function rebuildAll(bool $rebuildNodeAccess = FALSE, bool $rolesOnly = FALSE): void {
    if (!$rolesOnly) {
      $danse = NULL;
      $wasPaused = FALSE;
      if ($this->moduleHandler->moduleExists('danse')) {
        /** @var \Drupal\danse\Service $danse */
        // @phpstan-ignore-next-line
        $danse = \Drupal::service('danse.service');
        $wasPaused = $danse->isPaused();
        $danse->pause();
      }

      foreach ($this->roleStorage->loadMultiple() as $role) {
        $this->revokePermissions($role->id());
      }

      if ($this->groupRoleStorage !== NULL) {
        foreach ($this->groupRoleStorage->loadMultiple() as $role) {
          $this->revokeGroupPermissions($role->id());
        }
      }

      $contentAccessAvailable = $this->moduleHandler->moduleExists('content_access');
      if ($contentAccessAvailable) {
        $contentAccessConfig = $this->configFactory->getEditable('content_access.settings');
        $contentAccessConfig->set('content_access_node_type', [])->save();
      }
    }
    else {
      $contentAccessAvailable = FALSE;
    }

    foreach ($this->moduleHandler->getModuleList() as $module) {
      $this->createAndGrant($module->getName(), $contentAccessAvailable, $rolesOnly);
    }

    if ($rebuildNodeAccess) {
      node_access_rebuild();
    }

    if (!$rolesOnly && !$wasPaused && $danse !== NULL) {
      $danse->resume();
    }
  }

}
