<?php

namespace Drupal\roles_permission_builder\Drush\Commands;

use Drupal\roles_permission_builder\Builder;
use Drush\Attributes\Command;
use Drush\Attributes\Option;
use Drush\Attributes\Usage;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush command file.
 */
class BuildCommands extends DrushCommands {

  /**
   * Constructs the RolePermissionCommands object.
   *
   * @param \Drupal\roles_permission_builder\Builder $builder
   *   Roles and permissions service.
   */
  public function __construct(
    protected Builder $builder,
  ) {
    parent::__construct();
  }

  /**
   * Return an instance of these Drush commands.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   *
   * @return \Drupal\roles_permission_builder\Drush\Commands\BuildCommands
   *   The instance of Drush commands.
   */
  public static function create(ContainerInterface $container): BuildCommands {
    return new BuildCommands(
      $container->get('roles_permission_builder.roles_and_permissions')
    );
  }

  /**
   * Rebuild all roles and permissions.
   */
  #[Command(name: 'roles-permissions:rebuild', aliases: [])]
  #[Option(name: 'full', description: 'Rebuild node access table as well.')]
  #[Option(name: 'roles', description: 'Only ensure all defined roles.')]
  #[Usage(name: 'roles-permissions:rebuild', description: 'Rebuild all roles and permissions.')]
  public function rebuild(array $options = ['full' => FALSE]): void {
    $this->builder->rebuildAll($options['full'], $options['roles']);
  }

}
